using Microsoft.AspNetCore.Mvc;
using PlanetsApp.Enums;
using PlanetsApp.Models;
using static PlanetsApp.Enums.PlanetStatus;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Linq;
using PlanetsApp.Data;
using Microsoft.EntityFrameworkCore;

namespace PlanetsApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlanetsController : ControllerBase
    {
        private DataContext dataContext;

        public PlanetsController(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        [HttpGet]
        public async Task<ActionResult<List<Planet>>> GetAllPlanets()
        {
            return Ok(await dataContext.PlanetsTable.ToListAsync());
        }

        [HttpPost]
        public async Task<ActionResult<List<Planet>>> AddPlanet([FromBody]Planet planet)
        {
            dataContext.PlanetsTable.Add(planet);
            await dataContext.SaveChangesAsync();
            return Ok(await dataContext.PlanetsTable.ToListAsync());
        }

        [HttpPut]
        public async Task<ActionResult<UpdatePlanetResponse>> UpadetePlanet([FromBody] UpdatePlanetRequest request)
        {
            var match = await dataContext.PlanetsTable.FindAsync(request.PlanetId);
            if(match == null)
            {
                return BadRequest("Planet not found");
            }
            match.Description = request.PlanetDescription;
            match.Status = request.PlanetStatus;

            dataContext.SaveChanges();
            return Ok(new UpdatePlanetResponse() { Success = true });
        }
    }
}