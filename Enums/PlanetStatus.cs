﻿namespace PlanetsApp.Enums
{
    public class PlanetStatus
    {
        public enum Status
        {
            OK,
            NOT_OK,
            TODO,
            EN_ROUTE
        }
    }
}
