﻿using Microsoft.EntityFrameworkCore;
using PlanetsApp.Models;

namespace PlanetsApp.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options) { }

        public DbSet<Planet> PlanetsTable { get; set; }
    }
}
