﻿using PlanetsApp.Enums;

namespace PlanetsApp.Models
{
    public class UpdatePlanetRequest
    {
        public int PlanetId { get; set; }
        public string PlanetDescription { get; set; }
        public PlanetStatus.Status PlanetStatus { get; set; }
    }
}
