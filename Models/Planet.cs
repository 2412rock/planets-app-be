﻿using PlanetsApp.Enums;

namespace PlanetsApp.Models
{
    public class Planet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public PlanetStatus.Status Status { get; set; }
        public string Image { get; set; }

        public string CaptainName { get; set; }

        public string Robots { get; set; }


    }
}
