﻿namespace PlanetsApp.Models
{
    public class UpdatePlanetResponse
    {
        public bool Success { get; set; }
    }
}
